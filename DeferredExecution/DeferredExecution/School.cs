﻿using System.Collections;
using System.Collections.Generic;

namespace DeferredExecution
{
    public class School : IEnumerable<Student>
    {
        private List<Student> _students = new List<Student>();

        public void Add(Student student)
        {
            _students.Add(student);
        }

        public IEnumerator<Student> GetEnumerator()
        {
            return new StudentEnumerator(_students);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        private class StudentEnumerator : IEnumerator<Student>
        {
            private List<Student> _students = null;
            private int _pos = -1;

            public StudentEnumerator(List<Student> students)
            {
                _students = students;
            }

            public Student Current
            {
                get { return _students[_pos]; }
            }

            object System.Collections.IEnumerator.Current
            {
                get { return Current; }
            }

            public bool MoveNext()
            {
                return ++_pos < _students.Count;
            }

            public void Reset() { }
            public void Dispose() { }
        }
    }
}