﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace LazyOperators
{
    public class WhereEnumerator<T> : IEnumerable<T>, IEnumerator<T>
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private IEnumerable<T> _sourceEnumerable;

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private T _current;

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Func<T, bool> _predicate;

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private IEnumerator<T> _sourceEnumerator;
        
        public WhereEnumerator(IEnumerable<T> sourceEnumerable, Func<T, bool> predicate)
        {
            _sourceEnumerable = sourceEnumerable;

            _predicate = predicate;

            _sourceEnumerator = _sourceEnumerable.GetEnumerator();

            _current = default(T); // Or _sourceEnumerator.Current;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return this;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public T Current
        {
            get { return _current; }
        }

        object IEnumerator.Current
        {
            get { return Current; }
        }

        public bool MoveNext()
        {
            while (_sourceEnumerator.MoveNext())
                if (_predicate(_sourceEnumerator.Current))
                    return true;

            return false;
        }

        public void Reset() { }
        public void Dispose() { }
    }
}