﻿using System;
// using System.Linq;
// using GreedyOperators;
using LazyOperators;
using System.Diagnostics;

namespace DeferredExecution
{
    class Program
    {
        private static School OurSchool = new School
        {
            new Student { Name = "Sathyaish" },
            new Student { Name = "John" },
            new Student { Name = "Carol" },
            new Student { Name = "Peter" }
        };

        static void Main(string[] args)
        {
            var query = from student in OurSchool
                        where student.Name.StartsWith("S")
                        select student;

            foreach (var student in query)
                Console.WriteLine(student.Name);

            Debugger.Break();
        }
    }
}
