﻿using System;
using System.Collections.Generic;

namespace GreedyOperators
{
	public static class GreedyExtensions
	{
        public static IEnumerable<T> Where<T>(this IEnumerable<T> source, 
            Func<T, bool> predicate)
        {
            var list = new List<T>();

            foreach (var t in source)
                if (predicate(t))
                    list.Add(t);

            return list;
        }
	}
}
