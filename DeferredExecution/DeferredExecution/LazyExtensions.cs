﻿using System;
using System.Collections.Generic;

namespace LazyOperators
{
    public static class LazyExtensions
    {
        public static IEnumerable<T> Where<T>(this IEnumerable<T> source, 
            Func<T, bool> predicate)
        {
            return new WhereEnumerator<T>(source, predicate);
        }
    }
}
