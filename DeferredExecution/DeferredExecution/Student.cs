﻿namespace DeferredExecution
{
    [System.Diagnostics.DebuggerDisplay("Student = {Name}")]
    public class Student
    {
        public string Name { get; set; }
    }
}
